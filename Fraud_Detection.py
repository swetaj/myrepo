#!/usr/bin/env python
# coding: utf-8

# In[1]:



# In[46]:


# !pip install -U imbalanced-learn


# In[63]:


import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

# Sampling Methods
from imblearn.over_sampling import RandomOverSampler, SMOTE
from imblearn.under_sampling import RandomUnderSampler, NearMiss

# Training/Testing Split
from sklearn.model_selection import train_test_split

# Dimensionality Reduction
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

# Simple ML models
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC

# Cross Validation
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedStratifiedKFold

# Ensemble Learning
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import StackingClassifier



# In[3]:


df = pd.read_csv('creditcard.csv')
df.head()


# In[4]:


df.columns


# In[5]:


# Initial Diagnostics


# In[6]:


# df.info()


# In[7]:


# df.describe()


# In[8]:



df['Class'].value_counts(normalize=True)


# In[9]:



df['Amount'].describe()


# In[10]:


# sns.histplot(data=df, x='Amount').set(xscale="log")


# In[11]:


# Data Cleaning


# In[12]:


plt.figure(figsize=(12,8))
sns.heatmap(df.isna().T)
plt.xlabel("Feature")
plt.ylabel("Observations")


# In[13]:


# outliers

sns.boxenplot(data=df, x="Amount")
plt.show()


# In[14]:


# Observations:
# we can observe an non-neglible number of outliers on the upper end of the distribution.
# It would denotes transactions with high amounts in the order of thousands of dollars.
# We would assess the effect of this skewed distrbution when building the predictive models
# in terms of feature transformation or selecting model robust to such feature types.


# In[15]:


df.duplicated().value_counts()


# In[16]:


# 283726 duplicate rows, and so we proceed in removing them from the dataset


# In[17]:


df.drop_duplicates(keep='last', inplace=True)


# ## Correlation Analysis

# - used to study the strength of a relationship between two, numerically measured, continuous variables
# - useful when a researcher wants to establish if there are possible connections between variables.
# - three types:
#         - Positive Correlation: both variables change in the same direction.
#         - Neutral Correlation: No relationship in the change of the variables.
#         - Negative Correlation: variables change in opposite directions.

# In[18]:


df.corr().round(2)


# In[19]:


plt.figure(figsize=(12, 8))
matrix = df.corr().round(2)
mask = np.triu(np.ones_like(matrix, dtype=bool))
sns.heatmap(matrix, annot=True, vmax=1, vmin=-1, 
            center=0, cmap='vlag', mask=mask)
plt.show()


# observe:
# - There are very few correlated variables as we would expect after the feature transformation. 
# - The two meaningful features that are Time and Amount 
# - have some relative corelation with some variables with coefficients approximating 0.4. 
# - With such low values, it would be quite difficult to imply with any certainty a correlation between any of them. 
# - It also indicates that there would be very low incidence of any colinearity within our data
# 

# - filters those pairs with correlation coefficients above 0.2 as a threshold.

# In[20]:


# Filtering those with some signigicant correlation coefficients
matrix = df.corr()
matrix = matrix.unstack()
matrix = matrix[abs(matrix) >= 0.2]

# print(matrix)


# # Q1 How does Amount's distribution behaves across classes?

# In[21]:


# we first split our dataset by class types
# in other words fraudulent and non-fraudulent transaction. 
# We then proceeded in plotting the histogram side by side to observe an noteworthy behavior. 
# In doing so, the non-fraud transactions were heavily right skewed making
# it quite difficult to compare the plots. 
# To solve this issue, we used a logarithmic transformation 
# making it easier to see and thus, evaluate any similarities and differences.


# In[22]:


# Splitting data by fraud class
df_no_fraud = df[df['Class'] == 0]
df_fraud = df[df['Class'] == 1]


# In[27]:


# Histogram for Amount Distribution per class
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(19, 15))
fig.suptitle('Distribution of Transaction Amount across Class')
sns.histplot(ax=ax1, data=df_no_fraud, x='Amount', stat='probability')
sns.histplot(ax=ax2, data=df_fraud, x='Amount', stat='probability')
plt.show()


# # logarithmic scales

# - show relative values rather than absolute ones. Indeed, 2 minus 1 would be shown similarly as 9999 minus 9998
# - we are dealing with percentages here
# 
# 

# In[28]:


# Histogram for Amount Distribution per class after log transformation
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 8))
fig.suptitle('Distribution of Transaction Amount across Class')
sns.histplot(ax=ax1, data=df_no_fraud, x='Amount', stat='probability').set(xscale="log")
sns.histplot(ax=ax2, data=df_fraud, x='Amount', stat='probability').set(xscale="log")
plt.show()


# - depict the order of growth of transaction value
# - Both distribution depict a similar trajectory
# - with most transactions being on the lower end of the graph. 
# - It stays consistent with the mean value found at USD 88 
# - even with max values averaging USD 20,000.
# 

# # Q2: Are there any noteworthy point in time where fraud occured?

# In[38]:


plt.figure(figsize=(8,8))
sns.scatterplot(data=df_fraud, x='Time', y='Amount')
plt.show()


# - it does not appear that there is a clustering pattern on a time interval. 
# - So, we would assume that fraud occured across time quite randomly.

# # Class Imbalance

# - As we observed in our diagnotics, there is a stark imbalance between classes 
# - of transactions with fraud only making up 0.2% of all transaction statuses. 
# - It poses an issue in terms of building an effective machine model to predict 
# - if there is fraud given the limited pool of examples to train. 
# - With the minority class being so small, 
# - we would expect very poor performance on the important task of detecting fraud transactions. 
# - In that vein, we will use different sampling methods (Undersampling & Oversampling) to tackle this problem.

# In[51]:


X = df.drop(['Class'], axis=1)
y = df['Class']


# oversampling approach:
# - It balances the data by replicating the minority class samples.

# - SMOTE (Synthetic Minority Oversampling Technique) is an oversampling approach on the minority class. In context, it would mean to randomly increase fraud examples by "artificially" replicating to have a more balanced class distribution. 

# In[52]:


oversample = SMOTE()


# In[53]:


##SMOTE
oversample = SMOTE()
X_sm, y_sm = oversample.fit_resample(X, y)

print(f'''Shape of X before SMOTE: {X.shape}
Shape of X after SMOTE: {X_sm.shape}''')

print('\nBalance of positive and negative classes (%):')
y_sm.value_counts(normalize=True) * 100


# In[ ]:





# oversampling:
# - Near-Miss Algorithm is an undersampling approach on the majority class.
# - we select examples to keep out of the training set based on the distance of majority class examples to minority class examples.

# In[56]:


##Near Miss Algorithm
nr = NearMiss()

X_nmiss, y_nmiss = nr.fit_resample(X, y)

print(f'''Shape of X before NearMiss Algorithm: {X.shape}
Shape of X after NearMiss Algorithm: {X_nmiss.shape}''')

print('\nBalance of positive and negative classes (%):')
y_nmiss.value_counts(normalize=True) * 100


# # 
# - With the risk of overfitting with oversampling and the possibility to lose valuable information from undersampling, we will also consider combining both to rebalance the distribution.

# In[57]:


## Combined Random Sampler
over = RandomOverSampler(sampling_strategy=0.5)
under = RandomUnderSampler(sampling_strategy=0.8)

# Oversampling minority class
X_over, y_over = over.fit_resample(X, y)

# Comine with under sampling 
X_combined_sampling, y_combined_sampling = under.fit_resample(X_over, y_over)
print(f'''Shape of X before Combined Random Sampler: {X.shape}
Shape of X after Combined Random Sampler: {X_combined_sampling.shape}''')

print('\nBalance of positive and negative classes (%):')
y_combined_sampling.value_counts(normalize=True) * 100


# # Dimensionality Reduction

# - we will use dimensionality reduction to trim down the number of features we have. 
# - Dimensionality reduction encapsulates the techniques reducing the input variables in our training data. 
# - In doing so, we hope to a have simpler, but effective machine learning model structure and avoid any potential case of overfitting. 
# - We will be testing three different methods from Linear Algebra: PCA, SVD, and LDA on a simple Logistic Regression model and pick the best performing

# In[60]:


# Defined the final split
X, y = X_combined_sampling, y_combined_sampling

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.33, random_state=42)


# - PCA (Principal Component Analysis) takes data with m-columns to be projected to a subspace with n-features (n < m) while preserving the crucial information from the original data; 
# - in other words, PCA attempts in finding the principal components (or features) as its names denotes.
# - Further information: https://machinelearningmastery.com/calculate-principal-component-analysis-scratch-python/

# In[61]:


dimred_steps = dict()


# In[66]:


##Principal Component Analysis
# Define the pipeline
pca_steps = [('pca', PCA()), 
             ('m', LogisticRegression(solver='liblinear'))]

# Create & Evaluate model
dimred_steps.update({'PCA': pca_steps})


# In[ ]:


dimred_steps


# In[69]:


##Singular Value Decomposition
# Define the pipeline
svd_steps = [('svd', TruncatedSVD()), 
             ('m', LogisticRegression())]

# Create & Evaluate model
dimred_steps.update({'SVD': svd_steps})


# In[70]:


dimred_steps


# In[71]:


##Linear Discriminant Analysis
# Define the pipeline
lda_steps = [('lda', LinearDiscriminantAnalysis()), 
             ('m', LogisticRegression())]

dimred_steps.update({'LDA': lda_steps})


# In[72]:


dimred_steps


# In[75]:


cv = RepeatedStratifiedKFold(n_splits=3, n_repeats=1, random_state=1)
for name, steps in dimred_steps.items():
    model = Pipeline(steps=steps)
    n_scores = cross_val_score(model, X_train, y_train, 
                               scoring='average_precision', cv=cv, 
                               n_jobs=-1)
    # report performance
    print(name+' - Average Precision Score: %.3f' % (np.mean(n_scores)))


# In[78]:


cross_val_score(model, X_train, y_train, 
                               scoring='average_precision', cv=cv, 
                               n_jobs=-1)


# # Machine Learning models:

# In[79]:


# Dictionary to store model structures
model_steps = dict()


# In[80]:


## Logistic Regression
logreg_steps = [('lda', LinearDiscriminantAnalysis()), 
                ('m', LogisticRegression())]
model_steps.update({'Logistic Regression':logreg_steps})


# In[81]:


## k-Nearest Neighbors
knn_steps = [('lda', LinearDiscriminantAnalysis()),  
             ('m', KNeighborsClassifier(n_neighbors=2))]
model_steps.update({'k-Nearest Neighbors':knn_steps})


# In[82]:


## Stochastic Gradient Descent
sgd_steps = [('lda', LinearDiscriminantAnalysis()),  
             ('m', SGDClassifier())]
model_steps.update({'Stochastic Gradient Descent':sgd_steps})


# In[83]:


## Decision Tree
tree_steps = [('lda', LinearDiscriminantAnalysis()), 
               ('m', DecisionTreeClassifier())]
model_steps.update({'Decision Tree':tree_steps})


# In[84]:


# Checking the dictionary of models
model_steps


# In[85]:


# Cross-Validation
cv = RepeatedStratifiedKFold(n_splits=5, n_repeats=2)
for name, model_steps in model_steps.items():
    model = Pipeline(steps=model_steps)
    n_scores = cross_val_score(model, X_train, y_train, 
                               scoring='average_precision', cv=cv, 
                               n_jobs=-1)
    # report performance
    print(name+' - Average Precision Score: %.3f (%.3f)' % (np.mean(n_scores), 
                                                            np.std(n_scores)))


# # Machine Learning - Ensemble Methods

# - extend our work in mahcine learning to incorporate ensemble methods. 
# - We generated simple models and compared the scores which appear to be quite satisfactory with the 
# - lowest at 0.974 for the Average Precision Score. However, we may want more stability and less variation in our predictive algorithm;  - it is where ensemble techniques come in. Most often, they act as a superposer of multiple models throughout a variety of ways and thus, bolster their predictive power. 

# In[86]:


# Dictionary to store ensemble model structures
ensemble_models = dict()


# In[89]:


## Random Forest
rf_steps = [('lda', LinearDiscriminantAnalysis()), 
               ('m', RandomForestClassifier())]
ensemble_models.update({'Random Forest':rf_steps})


# In[90]:


## Stochastic Gradient Boosting
boost_steps = [('lda', LinearDiscriminantAnalysis()), 
               ('m', GradientBoostingClassifier())]
ensemble_models.update({'Stochastic Gradient Boosting':boost_steps})


# In[91]:


## StackingClassifier
estimators = [
    ('logreg', LogisticRegression()),
    ('knn', KNeighborsClassifier(n_neighbors=2)),
    ('sdg', SGDClassifier()),
     
]
stack_steps = [('lda', LinearDiscriminantAnalysis()), 
               ('m', StackingClassifier(estimators=estimators, 
                         final_estimator=DecisionTreeClassifier()))]
ensemble_models.update({'StackingClassifier':stack_steps})


# In[92]:


ensemble_models


# In[93]:


# Cross-Validation & model building
cv = RepeatedStratifiedKFold(n_splits=3, n_repeats=1)
for name, model_steps in ensemble_models.items():
    model = Pipeline(steps=model_steps)
    n_scores = cross_val_score(model, X_train, y_train, 
                               scoring='average_precision', cv=cv, 
                               n_jobs=-1)
    # report performance
    print(name+' - Average Precision Score: %.3f (%.3f)' % (np.mean(n_scores), 
                                                            np.std(n_scores)))


# observation:
# - Based on the numbers obtained, the Random Forest model was on par with the two best individual models, that are K-Nearest Neighbors and Decision Tree. 
# - The Stochastic Gradient Boosting outperformed Logistic Regression and Stochastic Gradient Descent. 
# - In contrast, the staking model was aorse than all of the above. Moving forward, we will evaluate the performance of the top 3 using the testing set.

# # Model Performance Evaluation

# - AUPRC (Area Under the Precision-Recall Curve) focuses on finding the positive examples; in other words, the fraudulent transactions in our case.

# In[95]:


# The top 3 models
models_top_3 = {}
models_top_3.update({'k-Nearest Neighbors':knn_steps})
models_top_3.update({'Decision Tree Classifier':tree_steps})
models_top_3.update({'Random Forest':rf_steps})


# In[96]:


# Cross Validation
cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3)
for name, model_steps in models_top_3.items():
    model = Pipeline(steps=model_steps)
    n_scores = cross_val_score(model, X_test, y_test, 
                               scoring='average_precision', cv=cv, 
                               n_jobs=-1)
    # report performance
    print(name+' - Average Precision Score: %.4f (%.4f)' % (np.mean(n_scores), 
                                                            np.std(n_scores)))


# In[ ]:





# In[ ]:





# - Though k-Nearest Neighbors and Random Forest had the highest average AUPRC, the Decision Tree Classifier had the lowest standard deviation. It would imply that this model is more stable while predicting more than 99% of all fraudulent cases given. So, we will present the Decision Tree Classifier as our final model

# In[ ]:




