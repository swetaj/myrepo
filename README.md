# **SJ's Portfolio:**


# [Project 1: Fraud Detection](https://gitlab.com/swetaj/myrepo/-/blob/main/Fraud_Detection.py)

 - Initial Analysis: Target Variable Analysis, Predictor analysis.
 - Data Cleaning: Missing Values, Outliers, Duplications.
 - Correlation Analysis: Correlation matrix, strong relations.
 - Exploratory Analysis:
     1. Behavour of amount distributed across classes.
     2. Pattern of occurance of fraud over period of time.
 - Handeling Class imbalance:
     1. SMOTE:
     2. Near Miss Algorithm
     3. Combined Random sampler
 - Dimensionality Reduction:
     1. PCA (Principle Component Analysis)
     2. SVD (Singular Value Decomposation)
     3. LDA (Linear Discreminant Analysis)
 - Models:
     1. Logistic Regression
     2. k-Nearest Neighbour 
     3. Decision Tree
     4. Stocastic Gradient Decent
     5. Random Forest
     6. Stocastic Gradient Boosting
     7. Stacking Classifier
 - Mocdel Performance Evaluation:
     1. Top three Models
     2. Cross- Validation
     
  *******************************************************************************************************************************

# [Project 2: Stock Market Prediction using LSTM](https://gitlab.com/swetaj/myrepo/-/blob/main/Stock_Market_Prediction_LSTM.py)

 - Analyze Basic Attributes: Change in price over time.
 - Caculate Moving Average.
 - Analyze Risk: Daily return of the stock on average.
     1. Create Histogram.
     2. Create kde plot.
 - Find correlation between different stocks
 - Find how much value do we put at risk by investing in a particular stock.
 - Predict future stock behavior: Closing Price of APPLE using LSTM.

  *******************************************************************************************************************************

# [Project 3: Bank Customer Churn Prediction](https://gitlab.com/swetaj/myrepo/-/blob/main/Bank_Customer_Churn.py)

- Data set review & preparation
    - Missing values
    - Count for each variable
    - Drop unwanted the columns
    - Check variable data types
- Exploratory Data Analysis
    - Understanding how given attributes relate to'Exit' status.
    - Review the 'Status' relation with categorical variables
    - Relations based on the continuous data attributes
- Feature engineering
    - Split Train, Test Data
- Data prep for model fitting
    - One hot encode the categorical variables
    - minMax scaling the continuous variables
    
- Model fitting and selection
    - Logistic regression in the primal space and with different kernels
    - SVM in the primal and with different Kernels
    - Ensemble models
- Review best model fit accuracy
- Test model prediction accuracy on test data
- Conclusion

  *******************************************************************************************************************************

# [Project 4: Sentiment Analysis and Topic Modeling](https://gitlab.com/swetaj/myrepo/-/blob/main/Sentiment_Analysis_and_Topic_Modeling.py)

- Data set review & preparation
    - Clean Text by removing digits, punctuation, wide spaces, newline etc
    - Correct Spellings

- Sentiment Analysis
    - Find polarity 
    - Find subjectivity
    - Visualize Result

- Prepare data for Topic Modeling
    - Create Corpus
    - Find all the nouns and adjectives 
    - Create Document-Term Matrix

- Model Creation
    - CountVectorizer
    - Visualize Result


  *******************************************************************************************************************************



