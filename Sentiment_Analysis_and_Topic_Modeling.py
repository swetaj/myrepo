#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import re, string, copy

import warnings
warnings.filterwarnings('ignore')

import matplotlib.pyplot as plt

game_overview = pd.read_csv("game_overview.csv")
train = pd.read_csv("train.csv")
train.head()

data = pd.merge(game_overview, train, "left", 'title')

data.shape


# In[3]:


title = data['title'].dropna().unique().tolist()
title


# In[4]:


# combine all the lists

def singleText(list_of_text):
    combinedtext = ' '.join(list_of_text)
    return combinedtext

def unique(list1):
    unique_list = []     
    for x in list1:
        if x not in unique_list:
            unique_list.append(x)
    return unique_list

def clean_data(text):
    text = text.lower()
    text = re.sub('\w*\d\w*', '' , text)
    text = re.sub('[%s]' % re.escape(string.punctuation), '' , text)
    text = re.sub('\[.*?\]', '' , text)
    text = re.sub('[''""...]', '' , text)
    text = re.sub('\n', '' , text)
    return text


# In[5]:


cleandf = pd.DataFrame(columns=['title', 'user_review'])
newdf=pd.DataFrame()
finaldf = pd.DataFrame()
j=0
for i in title:
    
    df = data.loc[(data['title'] == i) & (data['user_suggestion'] == 0)]
    df = df[['title', 'user_review']]
    
    combined_data = df.loc[df['title'] == i].apply(lambda x: singleText(x))    
    newdf['title'] = [' '.join(unique(combined_data['title'].split(" ")))]
    newdf['user_review'] = df.loc[df['title'] == i].apply(lambda x: singleText(x))['user_review']
    finaldf = finaldf.append(newdf)
    
    df = finaldf.reset_index()
    df['user_review'][0] = str(clean_data(df['user_review'][0]))


# In[6]:


df = df[['title', 'user_review']]
df.head(2)


# # Sentiment Analysis
# 

# In[8]:


from textblob import TextBlob

pol = lambda x: TextBlob(x).sentiment.polarity
sub = lambda x: TextBlob(x).sentiment.subjectivity
spel= lambda x: TextBlob(x).correct

df['polarity'] = df['user_review'].apply(pol)
df['subjectivity'] = df['user_review'].apply(sub)
df['CleanUR'] = df['user_review'].apply(spel)[0]


# In[9]:



for index, game in enumerate (df.index):
    # For each game, plot a point
    x=df['polarity'].loc[game]
    y=df['subjectivity'].loc[game]
    plt.scatter(x, y)
    plt.text( x+0.001, y+0.001, df['title'][index], fontsize=10 )
    
plt.title('Sentiment Analysis', fontsize=20)
plt.xlabel("Negative <------------------> Positive", fontsize=15)
plt.ylabel("Facts <------------------> Opinions", fontsize=15)
plt.show()


# In[10]:


for index, game in enumerate (df.index):
    # For each game, plot a point
    x=df['polarity'].loc[game]
    y=df['subjectivity'].loc[game]
    plt.scatter(x, y)
plt.title('Sentiment Analysis', fontsize=20)
plt.xlabel("Negative <------------------> Positive", fontsize=15)
plt.ylabel("Facts <------------------> Opinions", fontsize=15)
plt.show()


# # Topic Modeling

# In[11]:


gamelist = ['Sakura Clicker', 'Fractured Space', 'Path of Exile', 'War Thunder', 'Ring of Elysium', 
'Yu-Gi-Oh! Duel Links', 'SMITE®', 'Brawlhalla', 'World of Tanks Blitz', 'DCS World Steam Edition',
'School of Dragons', 'Cuisine Royale', 'World of Guns: Gun Disassembly']


# In[12]:


data = data.loc[data['title'].isin(gamelist)]


# In[13]:


data.groupby(['title']).count().sort_values('user_review', ascending=False).head(10)


# In[14]:


# combine all the lists
title = data['title'].dropna().unique().tolist()

def singleText(list_of_text):
    combinedtext = ' '.join(list_of_text)
    return combinedtext

def unique(list1):
    unique_list = []     
    for x in list1:
        if x not in unique_list:
            unique_list.append(x)
    return unique_list

def clean_data(text):
    text = text.lower()
    text = re.sub('\w*\d\w*', '' , text)
    text = re.sub(" \d+", '', text)
    text = re.sub('[%s]' % re.escape(string.punctuation), '' , text)
    text = re.sub('\[.*?\]', '' , text)
    text = re.sub('[''""...]', '' , text)
    text = re.sub('\n', '' , text)
    return text


# In[15]:


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction import text

extra_stop_words = ['game', 'room', 'nope', 'like', 'dont']
stop_words = text.ENGLISH_STOP_WORDS.union(extra_stop_words)
cv = CountVectorizer(stop_words=stop_words)


# In[16]:


cleandf = pd.DataFrame(columns=['title', 'user_review'])
newdf=pd.DataFrame()
finaldf = pd.DataFrame()
j=0
for i in title:
#     print("### i:", i)
#     print("j:", j)
    # user_suggestion = 0 : negative, user_suggestion = 1 : positive
    df = data.loc[(data['title'] == i) & (data['user_suggestion'] == 0)]
#     df = data.loc[(data['title'] == i) & (data['user_suggestion'] == 1)]

    df = df[['title', 'user_review']]
    
    ##  concatinating all the reviews into a single review
    combined_data = df.loc[df['title'] == i].apply(lambda x: singleText(x))    
    newdf['title'] = [' '.join(unique(combined_data['title'].split(" ")))]
    newdf['user_review'] = df.loc[df['title'] == i].apply(lambda x: singleText(x))['user_review']
    finaldf = finaldf.append(newdf)
    
    df = finaldf.reset_index()
    df['user_review'][0] = str(clean_data(df['user_review'][0]))
    
    datacv = cv.fit_transform(df['user_review'])
    dtm = pd.DataFrame(datacv.toarray(), columns = cv.get_feature_names())
    dtm.index = df.index
    dtm = dtm.T.reset_index()
#     print(dtm)
    # top30 common words for wordcloud
#     print(dtm.sort_values(j,ascending=False)[['index', j]].head(30))
    dtm.sort_values(j,ascending=False)
    j=j+1
df = df.drop(columns=['index'])


# In[17]:


from gensim import matutils, models
import scipy.sparse


# In[18]:


dtm = dtm.set_index('index')


# #### Topic modeling Noun and Adjective

# In[19]:


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction import text

from nltk import word_tokenize, pos_tag


# In[20]:


def nouns_Adj_func(text):
    isNounAdj = lambda x: x[:2]=='NN' or x[:2]=='JJ'
    tokenized = word_tokenize(text)
    allnounsAdj = [word for (word, pos) in pos_tag(tokenized) if isNounAdj(pos)]
    return ' '.join(allnounsAdj)


# In[21]:


data_nouns_adj = pd.DataFrame(df['user_review'].apply(nouns_Adj_func))
data_nouns_adj.index = df['title']
data_nouns_adj.head()


# In[24]:


data_nouns_adj = data_nouns_adj.tail(1)


# In[25]:


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction import text

extra_stop_words = ['game', 'room', 'nope', 'like', 'dont', 'games', 'play', 'just','dg4qiam', '2016i']
stop_words = text.ENGLISH_STOP_WORDS.union(extra_stop_words)
cvna = CountVectorizer(stop_words=stop_words)#, max_df=.8

data_cvna = cvna.fit_transform(data_nouns_adj['user_review'])
data_na_dtm = pd.DataFrame(data_cvna.toarray(), columns = cvna.get_feature_names())


# In[ ]:





# In[27]:


data_na_dtm


# In[28]:


data_na_dtm.T.sort_values(0, ascending = False).head(15)


# In[29]:



corpus = matutils.Sparse2Corpus(scipy.sparse.csr_matrix(data_na_dtm.transpose()))
id2word = dict((v,k) for k,v in cv.vocabulary_.items())


# In[30]:


# final nltk model for now
ldana = models.LdaModel(corpus=corpus, id2word=id2word,  num_topics=4,random_state=100,update_every=1,chunksize=100,passes=2,alpha='auto',per_word_topics=True)#num_topics=4, passes=20,
ldana.print_topics()


# In[31]:


from collections import Counter
import matplotlib.colors as mcolors

topics = ldana.show_topics(formatted=False)
data_flat = [w for w_list in ldana.print_topics() for w in w_list]
counter = Counter(data_flat)

out = []
for i, topic in topics:
    for word, weight in topic:
        out.append([word, i , weight, counter[word]])

df = pd.DataFrame(out, columns=['word', 'topic_id', 'importance', 'word_count'])        

# Plot Word Count and Weights of Topic Keywords
fig, axes = plt.subplots(2, 2, figsize=(16,10), sharey=True, dpi=160)
cols = [color for name, color in mcolors.TABLEAU_COLORS.items()]
for i, ax in enumerate(axes.flatten()):
    ax.bar(x='word', height="word_count", data=df.loc[df.topic_id==i, :], color=cols[i], width=0.5, alpha=0.3, label='Word Count')
    ax_twin = ax.twinx()
    ax_twin.bar(x='word', height="importance", data=df.loc[df.topic_id==i, :], color=cols[i], width=0.2, label='Weights')
    ax.set_ylabel('Word Count', color=cols[i])
    ax_twin.set_ylim(0, 0.030); ax.set_ylim(0, 3500)
    ax.set_title('Topic: ' + str(i), color=cols[i], fontsize=16)
    ax.tick_params(axis='y', left=False)
    ax.set_xticklabels(df.loc[df.topic_id==i, 'word'], rotation=30, horizontalalignment= 'right')
    ax.legend(loc='upper left'); ax_twin.legend(loc='upper right')

fig.tight_layout(w_pad=2)    
fig.suptitle('Word Count and Importance of Topic Keywords', fontsize=22, y=1.05)    
plt.show()


# In[32]:


# Word clouds


# In[33]:


from wordcloud import WordCloud


# In[35]:


dtm = dtm.sort_values(0, ascending=False)


# In[38]:


dtm


# In[40]:


counts = dtm.reset_index().set_index('index')
wordcloud = WordCloud(background_color="white").generate_from_frequencies(counts[0])
plt.imshow(wordcloud)
plt.axis("off")
plt.show()


# In[ ]:




